// Déclaration des constantes pour la grille
#define NUM_ROWS 9
#define NUM_COLUMNS 9

// Déclaration des constantes pour les directions de déplacement
#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3

// Fonction pour vérifier si un déplacement est valide
int deplacement_valide(int sourceX, int sourceY, int destX, int destY) {
    // Vérifier si les coordonnées de destination sont valides dans la grille
    if (destX < 0 || destX >= NUM_ROWS || destY < 0 || destY >= NUM_COLUMNS) {
        return 0;
    }
    
    // Vérifier si la case de destination est vide
    if (grid[destX][destY] != ' ') {
        return 0;
    }
    
    // Vérifier si le mouvement est en avant (pas de déplacement arrière)
    if ((destX > sourceX && sourceY == destY) || (destY > sourceY && sourceX == destX)) {
        return 1;
    }
    
    return 0;
}

// Fonction pour effectuer un déplacement
int effectuer_deplacement(int sourceX, int sourceY, int destX, int destY) {
    if (deplacement_valide(sourceX, sourceY, destX, destY)) {
        grid[destX][destY] = grid[sourceX][sourceY];
        grid[sourceX][sourceY] = ' ';
        return 1;
    }
    return 0;
}






////////////une autre 


// Déclaration des constantes pour la grille
#define NUM_ROWS 9
#define NUM_COLUMNS 9

// Déclaration des constantes pour les directions de déplacement
#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3

// Fonction pour vérifier si un déplacement est valide
int deplacement_valide(int sourceX, int sourceY, int destX, int destY) {
    // Vérifier si les coordonnées de destination sont valides dans la grille
    if (destX < 0 || destX >= NUM_ROWS || destY < 0 || destY >= NUM_COLUMNS) {
        return 0;
    }
    
    // Vérifier si la case de destination est vide
    if (grid[destX][destY] != ' ') {
        return 0;
    }
    
    // Vérifier si le mouvement est en avant (pas de déplacement arrière)
    if ((destX > sourceX && sourceY == destY) || (destY > sourceY && sourceX == destX)) {
        return 1;
    }
    
    return 0;
}

// Fonction pour effectuer un déplacement
int effectuer_deplacement(int sourceX, int sourceY, int destX, int destY) {
    if (deplacement_valide(sourceX, sourceY, destX, destY)) {
        grid[destX][destY] = grid[sourceX][sourceY];
        grid[sourceX][sourceY] = ' ';
        return 1;
    }
    return 0;
}





///// une autre part 

SDL_Event event;
int mouseX, mouseY;

while (gameRunning) {
    // Gestion des événements
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) {
            gameRunning = 0;
        } else if (event.type == SDL_MOUSEBUTTONDOWN) {
            if (event.button.button == SDL_BUTTON_LEFT) {
                mouseX = event.button.x;
                mouseY = event.button.y;
                
                // Utilisez les valeurs de mouseX et mouseY pour enregistrer les coordonnées
                // et effectuer les opérations souhaitées
            }
        }
    }
    
    // Le reste de votre boucle de jeu et de rendu
}

int clickedRow = mouseY / RECTANGLE_SIZE;
int clickedColumn = mouseX / RECTANGLE_SIZE;



while (gameRunning) {
    // Gestion des événements
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) {
            gameRunning = 0;
        } else if (event.type == SDL_MOUSEBUTTONDOWN) {
            if (event.button.button == SDL_BUTTON_LEFT) {
                switch (etatClics) {
                    case ATTENTE_PREMIER_CLIC:
                        premierClic.x = event.button.x;
                        premierClic.y = event.button.y;
                        etatClics = PREMIER_CLIC_EFFECTUE;
                        break;
                    case PREMIER_CLIC_EFFECTUE:
                        deuxiemeClic.x = event.button.x;
                        deuxiemeClic.y = event.button.y;
                        etatClics = DEUXIEME_CLIC_EFFECTUE;
                        break;
                    case DEUXIEME_CLIC_EFFECTUE:
                        // Réinitialiser les clics et revenir à l'état initial
                        premierClic.x = -1;
                        premierClic.y = -1;
                        deuxiemeClic.x = -1;
                        deuxiemeClic.y = -1;
                        etatClics = ATTENTE_PREMIER_CLIC;
                        break;
                }
            }
        }
    }

    // Le reste de votre boucle de jeu et de rendu
}



/////////////////////////


// ...

// Initialisation des variables
int nombreClics = 0;
Coordonnees clics[MAX_CLICS];
int boutonSourisEnfonce = 0; // Variable pour suivre l'état du bouton de la souris

// Boucle principale du jeu
while (gameRunning) {
    // Gestion des événements
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) {
            gameRunning = 0;
        } else if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT) {
            if (!boutonSourisEnfonce && nombreClics < MAX_CLICS) {
                clics[nombreClics].x = event.button.x;
                clics[nombreClics].y = event.button.y;
                nombreClics++;
                boutonSourisEnfonce = 1;
            }
        } else if (event.type == SDL_MOUSEBUTTONUP && event.button.button == SDL_BUTTON_LEFT) {
            boutonSourisEnfonce = 0;
        } else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_RETURN) {
            // Le joueur a appuyé sur la touche "Entrée"
            // Traitez les sélections des carrés enregistrées dans clics
            // Réinitialisez le compteur de clics pour une nouvelle sélection
            nombreClics = 0;
        }
    }

    // Effacer l'écran
    SDL_SetRenderDrawColor(renderer, 64, 49, 45, 255);
    SDL_RenderClear(renderer);

    // Dessiner les carrés et les sélections enregistrées (si nécessaire)
    // ...

    // Mettre à jour l'affichage
    SDL_RenderPresent(renderer);
}

// ...






// ...

// Initialisation des variables
int nombreClics = 0;
Coordonnees clics[MAX_CLICS];
int boutonSourisEnfonce = 0; // Variable pour suivre l'état du bouton de la souris

// Boucle principale du jeu
while (gameRunning) {
    // Gestion des événements
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) {
            gameRunning = 0;
        } else if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT) {
            if (!boutonSourisEnfonce && nombreClics < MAX_CLICS) {
                clics[nombreClics].x = event.button.x;
                clics[nombreClics].y = event.button.y;
                nombreClics++;
                boutonSourisEnfonce = 1;
            }
        } else if (event.type == SDL_MOUSEBUTTONUP && event.button.button == SDL_BUTTON_LEFT) {
            boutonSourisEnfonce = 0;
        } else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_RETURN) {
            // Le joueur a appuyé sur la touche "Entrée"
            // Traitez les sélections des carrés enregistrées dans clics
            // Réinitialisez le compteur de clics pour une nouvelle sélection
            nombreClics = 0;
        }
    }

    // Effacer l'écran
    SDL_SetRenderDrawColor(renderer, 64, 49, 45, 255);
    SDL_RenderClear(renderer);

    // Dessiner les carrés et les sélections enregistrées (si nécessaire)
    // ...

    // Mettre à jour l'affichage
    SDL_RenderPresent(renderer);
}

// ...










/////////////////////

// ...

int nombreClics = 0;
Coordonnees clics[MAX_CLICS]; // MAX_CLICS est la taille maximale du tableau

// Boucle principale du jeu
while (gameRunning) {
    // Gestion des événements
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) {
            gameRunning = 0;
        } else if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT) {
            if (nombreClics < MAX_CLICS) {
                clics[nombreClics].x = event.button.x;
                clics[nombreClics].y = event.button.y;
                nombreClics++;
            }
        } else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_RETURN) {
            // Le joueur a appuyé sur la touche "Entrée"
            // Traitez les sélections des carrés enregistrées dans clics
            // Réinitialisez le compteur de clics pour une nouvelle sélection
            nombreClics = 0;
        }
    }

    // Effacer l'écran
    SDL_SetRenderDrawColor(renderer, 64, 49, 45, 255);
    SDL_RenderClear(renderer);

    // Dessiner les carrés et les sélections enregistrées (si nécessaire)
    // ...

    // Mettre à jour l'affichage
    SDL_RenderPresent(renderer);
}

// ...







////////////////


// ...

int nombreClics = 0;
Coordonnees clics[MAX_CLICS]; // MAX_CLICS est la taille maximale du tableau

// Boucle principale du jeu
while (gameRunning) {
    // Gestion des événements
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) {
            gameRunning = 0;
        } else if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT) {
            if (nombreClics < MAX_CLICS) {
                clics[nombreClics].x = event.button.x;
                clics[nombreClics].y = event.button.y;
                nombreClics++;
            }
        } else if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_RETURN || event.key.keysym.sym == SDLK_SPACE) {
                // Le joueur a appuyé sur la touche "Entrée" ou la barre d'espace
                // Traitez les sélections des carrés enregistrées dans clics
                // Réinitialisez le compteur de clics pour une nouvelle sélection
                nombreClics = 0;
            }
        }
    }

    // Effacer l'écran
    SDL_SetRenderDrawColor(renderer, 64, 49, 45, 255);
    SDL_RenderClear(renderer);

    // Dessiner les carrés et les sélections enregistrées (si nécessaire)
    // ...

    // Mettre à jour l'affichage
    SDL_RenderPresent(renderer);
}

// ...









//////////////////////



// ...

int nombreClics = 0;
Coordonnees clics[MAX_CLICS]; // MAX_CLICS est la taille maximale du tableau

// Boucle principale du jeu
while (gameRunning) {
    // Gestion des événements
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) {
            gameRunning = 0;
        } else if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT) {
            if (nombreClics < MAX_CLICS) {
                clics[nombreClics].x = event.button.x;
                clics[nombreClics].y = event.button.y;
                nombreClics++;
            }
        } else if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_RETURN || event.key.keysym.sym == SDLK_SPACE) {
                // Le joueur a appuyé sur la touche "Entrée" ou la barre d'espace
                // Traitez les sélections des carrés enregistrées dans clics
                // Réinitialisez le compteur de clics pour une nouvelle sélection
                nombreClics = 0;
            }
        }
    }

    // Effacer l'écran
    SDL_SetRenderDrawColor(renderer, 64, 49, 45, 255);
    SDL_RenderClear(renderer);

    // Dessiner les carrés et les sélections enregistrées (si nécessaire)
    // ...

    // Mettre à jour l'affichage
    SDL_RenderPresent(renderer);
}

// ...
